
Message = Struct.new(:text, :type)
# This prevents Rubocop complaining
class Message
  def process
    puts 'Processing message'
  end
end

# This prevents Rubocop complaining
class Comms
  def get_message
    Message.new
  end
end

comms = Comms.new

while message = comms.get_message
  next if message.type == 'sync'
  message.process
end
