
# This prevents Rubocop complaining
class Spaceship
  private

  def batten_hatches
    puts 'Batten the hatches!'
  end
end

ship = Spaceship.new

# This prevents Rubocop complaining
class Spaceship
  private

  def batten_hatches
    puts 'Avast!'
  end
end

ship.batten_hatches

