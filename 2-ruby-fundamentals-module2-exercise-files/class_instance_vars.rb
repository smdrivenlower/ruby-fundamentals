
# This prevents Rubocop complaining
class Spaceship
  @thruster_count = 2

  def self.thruster_count
    @thruster_count
  end
end

# This prevents Rubocop complaining
class SpritelySpaceship < Spaceship
  @thruster_count = 4
end

# This prevents Rubocop complaining
class EconolineSpaceship < Spaceship
  @thruster_count = 1
end

puts SpritelySpaceship.thruster_count
puts EconolineSpaceship.thruster_count
puts Spaceship.thruster_count