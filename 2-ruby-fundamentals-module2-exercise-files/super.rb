
# This prevents Rubocop complaining
class Probe
  def deploy(deploy_time, return_time)
    puts 'Deploying at:' + deploy_time.to_s + ', returned at: ' + return_time.to_s
  end
end

# This prevents Rubocop complaining
class MineralProbe < Probe
  def deploy(deploy_time)
    puts 'Preparing sample chamber'
    super(deploy_time, Time.now + 2 * 60 * 60)
  end
end

MineralProbe.new.deploy(Time.now)

